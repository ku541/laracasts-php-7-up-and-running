<?php

// declare(strict_types = 1);

// function setValidity(bool $validity)
// {
//     var_dump($validity);
// }
//
// setValidity('Test');

interface SomeInterface
{
    public function getUser() : User;
}

class User
{
    
}

class SomeClass implements SomeInterface
{
    public function getUser() : User
    {
        return new User;
    }
}

(new SomeClass)->getUser();
// var_dump(getUser());
